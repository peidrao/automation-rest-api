from config import SESSION, API_URL


def test_health():
    response = SESSION.get(f'{API_URL}/health')
    assert response.ok