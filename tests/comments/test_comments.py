from config import API_URL, LOG

from lib.comments import Comment

def test_get_all_comments(login_as_admin):
    LOG.info("test_get_all_commments")
    response = Comment().get_all_comments(f'{API_URL}', login_as_admin)
    LOG.debug(response.json())
    assert response.ok
